# [repentance] web framework

[repentance] strives to be stand-alone, fully featured web framework for creating fast, responsive and reactive web applications.

## another framework? for real?

as much as i'd love to use something that already exists, i have not found anything that suits my needs.

my requirements were following:

# features

## high performance

[repentance] strives to allow you fast applications intuitively.

this is achieved by the following:

-	no virtual [dom] - no more slow updates or hogging system memory
-	language with no garbage collector or other runtime - bye bye to random [gc] lag spikes

## pleasant to work with

a very important aspect in the framework design is to make it intuitive and enjoyable to the developer.

for that, the framework has:

-	good programming language - the use of [rust] both guarantees a blazing performance *and* helps you to be sure that your code is bug-free, even in multi-threaded scenarios.
-	no [xml] - [xml] should have not made it past the year 2000. it's hard to read, hard to write and prone to errors of many kinds. instead, [repentance] has a custom, readable but powerful, mark-up language.

# features not yet done, but expected to arrive in the future

- server side rendering
- server-side-only components which are not present on the client at all
