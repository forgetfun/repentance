#![feature(proc_macro_hygiene, decl_macro)]
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]

#[macro_use]
extern crate rocket;

// mod warp_response;

use std::{
	env,
	path::{Path, PathBuf},
};

use conquer_once::{Lazy, OnceCell};
use repentance_proc_macros::xml;
use rocket::response::{content, Responder};
use rocket_contrib::serve::StaticFiles;

static SERVER_ROOT: OnceCell<PathBuf> = OnceCell::uninit();
static STATIC_FILE_ROOT: Lazy<PathBuf> =
	Lazy::new(|| SERVER_ROOT.get().unwrap().join(Path::new("static")));

#[get("/")]
fn index() -> impl Responder<'static> {
	content::Html(xml! {
		html [
			head [
				meta(charset = "UTF-8"),

				title ["Hello there!"],

				script("type" = "module") [r#"
					import init from "/static/wasm/index.js";

					init();
				"#],
			],
			body [
				div(id = "root"),
			],
		]
	})
}

fn init() {
	SERVER_ROOT.init_once(|| {
		env::args()
			.nth(1)
			.expect("please provide the server working directory as a first argument")
			.parse::<PathBuf>()
			.unwrap()
			.canonicalize()
			.unwrap()
	});
}

fn main() {
	init();

	rocket::ignite()
		.mount("/app", routes![index])
		.mount("/static", StaticFiles::from(STATIC_FILE_ROOT.as_ref()))
		.launch();
}
