use crate::structures::Element;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Element)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"<{name}{attributes}>{children}</{name}>",
			name = self.name,
			attributes = self.attributes.display_xml(),
			children = self.children.display_xml(),
		)
	}
}
