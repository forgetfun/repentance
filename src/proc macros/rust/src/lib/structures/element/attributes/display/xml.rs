use crate::structures::element::attributes::Attributes;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Attributes)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.items
			.iter()
			.try_for_each(|item| write!(f, " {}", item.display_xml()))
	}
}
