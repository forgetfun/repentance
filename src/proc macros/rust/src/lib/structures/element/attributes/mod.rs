mod attribute;
mod display;
mod optional;

use attribute::Attribute;
pub use optional::Optional;
use syn::{
	parenthesized,
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::Comma,
};

#[derive(Debug)]
pub struct Attributes {
	pub items: Punctuated<Attribute, Comma>,
}

impl Attributes {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Attributes {
	fn parse(input: ParseStream) -> syn::Result<Self> {
		let content;
		parenthesized!(content in input);
		let items = (content.parse_terminated(Attribute::parse))?;

		Ok(Self { items })
	}
}
