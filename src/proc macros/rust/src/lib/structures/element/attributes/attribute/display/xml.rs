use crate::structures::element::attributes::attribute::Attribute;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Attribute)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{name}={value}",
			name = self.name.display_xml(),
			value = self.value.display_xml(),
		)
	}
}
