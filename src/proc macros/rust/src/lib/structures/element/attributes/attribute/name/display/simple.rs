use crate::structures::element::attributes::attribute::name::Name;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Name)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match **self {
			Name::Ident(inner) => write!(f, "{}", inner),
			Name::LitStr(inner) => write!(f, "\"{}\"", inner.value()),
		}
	}
}
