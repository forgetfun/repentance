use crate::structures::element::attributes::Optional;
use pad_adapter::PadAdapter;
use repentance_core::facade;
use std::fmt::{Display, Write};

facade!(
	pub Simple<'a>(&'a Optional)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if let Some(attributes) = &****self {
			writeln!(f, "(")?;
			write!(PadAdapter::new(f), "{}", attributes.display_simple())?;
			write!(f, ")")?;
		}

		Ok(())
	}
}
