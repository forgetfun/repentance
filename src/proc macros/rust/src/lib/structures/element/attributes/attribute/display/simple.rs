use crate::structures::element::attributes::attribute::Attribute;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Attribute)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			r#"{}={}"#,
			self.name.display_simple(),
			self.value.display_simple()
		)
	}
}
