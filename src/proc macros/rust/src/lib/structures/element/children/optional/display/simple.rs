use crate::structures::element::children::Optional;
use pad_adapter::PadAdapter;
use repentance_core::facade;
use std::fmt::{Display, Write};

facade!(
	pub Simple<'a>(&'a Optional)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if let Some(children) = &****self {
			writeln!(f, " [")?;
			write!(PadAdapter::new(f), "{}", children.display_simple())?;
			write!(f, "]")?;
		}

		Ok(())
	}
}
