use super::super::Children;

use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Children)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.items
			.iter()
			.try_for_each(|item| write!(f, "{}", item.display_xml()))
	}
}
