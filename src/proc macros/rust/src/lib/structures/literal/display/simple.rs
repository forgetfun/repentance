use crate::structures::literal::Literal;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Literal)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "\"{}\"", match **self {
			Literal::Str(inner) => inner.value(),
		})
	}
}
