use proc_macro2::TokenStream;
use quote::{quote, ToTokens};

fn get_runtime_path() -> TokenStream {
	quote! { repentance_runtime }
}

pub fn element(name: &str, children: impl ToTokens) -> TokenStream {
	let runtime = get_runtime_path();

	quote!(
		{
			use #runtime::node_factories::element;

			element(
				c,
				#name,
				#children,
			)
		}
	)
}

pub fn text(value: &str) -> TokenStream {
	let runtime = get_runtime_path();

	quote!(
		{
			use #runtime::node_factories::text;

			text(
				c,
				#value,
			)
		}
	)
}

pub fn child_list_dyn(children: impl IntoIterator<Item = TokenStream>) -> TokenStream {
	let runtime = get_runtime_path();
	let mut tokens = TokenStream::new();

	for child in children {
		tokens.extend(quote!(
			&Into::<#runtime::mountable::impls::MountableNode>::into(
				Into::<web_sys::Node>::into(#child)
			) as &dyn #runtime::Mountable,

		));
	}

	quote!(&[#tokens])
}
