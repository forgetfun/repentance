#[macro_export]
macro_rules! facade {
	(
		$(
			#[$attr:meta]
		)*
		$vis:vis
		$name:ident
		$(<$($bounds:tt),*>)?
		(
			$type:ty
		)
	) => {
		$(
			#[$attr]
		)*
		$vis struct $name $(<$($bounds),*>)? ($type);

		impl $(<$($bounds),*>)?
			From<$type>
			for $name $(<$($bounds),*>)?
		{
			fn from(value: $type) -> Self { Self(value) }
		}

		impl $(<$($bounds),*>)?
			std::ops::Deref
			for $name $(<$($bounds),*>)?
		{
			type Target = $type;

			fn deref(&self) -> &Self::Target { &self.0 }
		}
	}
}
