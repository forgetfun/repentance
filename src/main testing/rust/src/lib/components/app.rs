use repentance_proc_macros::xml_runtime;
use repentance_runtime::{
	mountable::{impls::MountableNode, Mountable},
	node_factories::Context,
};
use web_sys::Node;

pub struct App {
	#[allow(dead_code)]
	element: MountableNode,
}

impl App {
	pub fn new(c: &Context) -> Self {
		let element = Into::<Node>::into(xml_runtime![
			div [
				h1 ["hello there!"],
				p ["this is an example app that has no real use apart from teaching me rust"],
				style ["
					p {
						color: blue;
					}
				"]
			]
		])
		.into();

		Self {
			element,
			// todo_adder,
		}
	}
}

impl Mountable for App {
	fn mount(&self, parent: &Node) { self.element.mount(parent); }
}
